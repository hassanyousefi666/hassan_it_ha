package test;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {

	public static void main(String[] args) {
		
		Rechteck r0 = new Rechteck();
		Rechteck r1 = new Rechteck();
		Rechteck r2 = new Rechteck();
		Rechteck r3 = new Rechteck();
		Rechteck r4 = new Rechteck();
		Rechteck r5 = new Rechteck(200,200,200,200);
		Rechteck r6 = new Rechteck( 800,400,20,20);
		Rechteck r7 = new Rechteck(800,450,20,20);
		Rechteck r8 = new Rechteck(850,400,20,20);
		Rechteck r9 = new Rechteck( 855,455,25,25);
		
		r0.setX(10);
		r0.setY(10);
		r0.setBreite(30);
		r0.setHoehe(40);
		r1.setX(25);
		r1.setY(25);
		r1.setBreite(100);
		r1.setHoehe(20);
		r2.setX(260);
		r2.setY(10);
		r2.setBreite(200);
		r2.setHoehe(100);
		r3.setX(5);
		r3.setY(500);
		r3.setBreite(300);
		r3.setHoehe(25);
		r4.setX(100);
		r4.setY(100);
		r4.setBreite(100);
		r4.setHoehe(100);
		
		System.out.println(r0.toString());
		System.out.println(r0.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]"));
		
		BunteRechteckeController brc = new BunteRechteckeController();
		brc.add(r0);
		brc.add(r1);
		brc.add(r2);
		brc.add(r3);
		brc.add(r4);
		brc.add(r5);
		brc.add(r6);
		brc.add(r7);
		brc.add(r8);
		brc.add(r9);
		System.out.println(brc.toString());
	}

}
